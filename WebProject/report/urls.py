from django.urls import path

from . import views

urlpatterns = [
    path('billionaires2020', views.billionaires2020, name='billionaires2020'),
    path('billionaires2021', views.billionaires2021, name='billionaires2021'),
    path('billionaires2022', views.billionaires2022, name='billionaires2022'),
    path('happiness', views.happiness, name='happiness'),
    path('happiness_top_2019', views.happiness_top_2019, name='happiness_top_2019'),
    path('happiness_top_2020', views.happiness_top_2020, name='happiness_top_2020'),
    path('happiness_top_2021', views.happiness_top_2021, name='happiness_top_2021'),
    path('covid', views.covid, name='covid')
]
