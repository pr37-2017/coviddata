import sys
import os
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json
from django.http import FileResponse
import pandas
import matplotlib.pyplot as plt


def get_happiness_plot(top_num):
	hap2019 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanHappiness2019.parquet.gzip")
	# print(hap2019)
	hap2020 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanHappiness2020.parquet.gzip")
	# print(hap2020)
	hap2021 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanHappiness2021.parquet.gzip")
	df1 = hap2019.head(top_num)
	df1.GDP = df1.GDP.mul(10)
	df2 = hap2020.head(top_num)
	df3 = hap2021.head(top_num)
	#ax = plt.subplot()
	df1.plot(x="country", y=["GDP", "score"], kind="bar", figsize=(9, 8))
	#plt.show()
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/HapTop52019" + ".png")
	plt.clf()
	plt.cla()
	df2.plot(x="country", y=["GDP", "score"], kind="bar", figsize=(9, 8))
	#plt.show()
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/HapTop52020" + ".png")
	plt.clf()
	plt.cla()
	df3.plot(x="country", y=["GDP", "score"], kind="bar", figsize=(9, 8))
	#plt.show()
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/HapTop52021"  + ".png")
	# plt.show()
	plt.clf()
	plt.cla()

def get_covid_plot(country):
	cov = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanCovidData.parquet.gzip")
	df = cov[cov["location"] == country]
	df = df[df['date'].isin(["2020-12-31", "2021-12-31", "2022-09-20"])]
	#print(df)
	df.plot(x="date", y=["total_cases_per_million", "total_deaths", "people_vaccinated", "people_fully_vaccinated"], kind="bar", figsize=(9, 8))
	plt.title(country)
	#plt.show()
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/covid" + country + ".png")
	# plt.show()
	plt.clf()
	plt.cla()

def get_billionaire_plot(country):
	bil2020 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanBillionaires2020.parquet.gzip")
	# print(bil2020)
	bil2021 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanBillionaires2021.parquet.gzip")
	# print(bil2021)
	bil2022 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanBillionaires2022.parquet.gzip")
	# print(bil2022)
	df2020 = bil2020[bil2020["Country"] == country]
	df1 = df2020.head(5)
	df2021 = bil2021[bil2021["Country"] == country]
	df2 = df2021.head(5)
	df2022 = bil2022[bil2022["Country"] == country]
	df3 = df2022.head(5)
	ax = plt.subplot()
	plt.scatter("Name", "NetWorth", data=df1)
	plt.title("Top 5 billionaires from " + country +" 2020.")
	plt.ylabel("billions")
	plt.setp(ax.get_xticklabels(), rotation=10, ha='right')
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/top5bil2020" + country + ".png")
	#plt.show()
	plt.clf()
	plt.cla()
	ax = plt.subplot()
	plt.scatter("Name", "NetWorth", data=df2)
	plt.title("Top 5 billionaires from " + country +" 2021.")
	plt.ylabel("billions")
	plt.setp(ax.get_xticklabels(), rotation=10, ha='right')
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/top5bil2021" + country + ".png")
	#plt.show()
	plt.clf()
	plt.cla()
	ax = plt.subplot()
	plt.scatter("Name", "NetWorth", data=df3)
	plt.title("Top 5 billionaires from " + country +" 2022.")
	plt.ylabel("billions")
	plt.setp(ax.get_xticklabels(), rotation=10, ha='right')
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/top5bil2022" + country + ".png")
	#plt.show()
	plt.clf()
	plt.cla()

def get_happiness_country_plot(country):
	hap2019 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanHappiness2019.parquet.gzip")
	# print(hap2019)
	hap2020 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanHappiness2020.parquet.gzip")
	# print(hap2020)
	hap2021 = pandas.read_parquet("/root/PycharmProjects/coviddata2/CleansedRawData/cleanHappiness2021.parquet.gzip")
	df2019 = hap2019[hap2019["country"] == country]
	df2019.GDP = df2019.GDP.mul(10)
	df2019.country = "2019"
	df2020 = hap2020[hap2020["country"] == country]
	df2020.country = "2020"
	df2021 = hap2021[hap2021["country"] == country]
	df2021.country = "2021"

	df2 = df2019.append(df2020, ignore_index=True)
	df = df2.append(df2021, ignore_index=True)

	df.plot(x="country", y=["GDP", "score", "corruption", "freedomOfChoice"], kind="bar", figsize=(9, 8))
	plt.title(country)
	plt.savefig("/root/PycharmProjects/coviddata2/Figures/hap" + country + ".png")
	plt.clf()
	plt.cla()


def billionaires2020(request):
    params = request.GET
    country = params['country']
    get_billionaire_plot(country)
    img = open("/root/PycharmProjects/coviddata2/Figures/top5bil2020" + country + ".png", 'rb')
    response = FileResponse(img)
    #fig.savefig(response)
    return response


#http://127.0.0.1:8000/report/billionaires2021?country=China
def billionaires2021(request):
    params = request.GET
    country = params['country']
    get_billionaire_plot(country)
    img = open("/root/PycharmProjects/coviddata2/Figures/top5bil2021" + country + ".png", 'rb')
    response = FileResponse(img)
    #fig.savefig(response)
    return response

def billionaires2022(request):
    params = request.GET
    country = params['country']
    get_billionaire_plot(country)
    img = open("/root/PycharmProjects/coviddata2/Figures/top5bil2022" + country + ".png", 'rb')
    response = FileResponse(img)
    #fig.savefig(response)
    return response

def happiness(request):
    params = request.GET
    country = params['country']
    get_happiness_country_plot(country)
    img = open("/root/PycharmProjects/coviddata2/Figures/hap" + country + ".png", 'rb')
    response = FileResponse(img)
    # fig.savefig(response)
    return response

def happiness_top_2019(request):
    params = request.GET
    num = params['top']
    get_happiness_plot(int(num))
    img = open("/root/PycharmProjects/coviddata2/Figures/HapTop52019"  + ".png", 'rb')
    response = FileResponse(img)
    return response

def happiness_top_2020(request):
    params = request.GET
    num = params['top']
    get_happiness_plot(int(num))
    img = open("/root/PycharmProjects/coviddata2/Figures/HapTop52020"  + ".png", 'rb')
    response = FileResponse(img)
    return response

def happiness_top_2021(request):
    params = request.GET
    num = params['top']
    get_happiness_plot(int(num))
    img = open("/root/PycharmProjects/coviddata2/Figures/HapTop52021"  + ".png", 'rb')
    response = FileResponse(img)
    return response


def covid(request):
    params = request.GET
    country = params['country']
    get_covid_plot(country)
    img = open("/root/PycharmProjects/coviddata2/Figures/covid" + country + ".png", 'rb')
    response = FileResponse(img)
    return response

