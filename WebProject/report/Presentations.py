import pandas
import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType
import seaborn as sns
import matplotlib.pyplot as plt


sns.set()
spark = SparkSession.builder\
	.master("local").appName("hdfs_test").getOrCreate()

pandas.set_option('display.max_columns', None)

caseclass2020 = pandas.read_parquet("../../CaseClassData/caseClass2020.parquet.gzip")


bil2020 = pandas.read_parquet("../../CleansedRawData/cleanBillionaires2020.parquet.gzip")
#print(bil2020)
bil2021 = pandas.read_parquet("../../CleansedRawData/cleanBillionaires2021.parquet.gzip")
#print(bil2021)
bil2022 = pandas.read_parquet("../../CleansedRawData/cleanBillionaires2022.parquet.gzip")
#print(bil2022)

cov = pandas.read_parquet("../../CleansedRawData/cleanCovidData.parquet.gzip")
print(cov)

hap2019 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2019.parquet.gzip")
#print(hap2019)
hap2020 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2020.parquet.gzip")
#print(hap2020)
hap2021 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2021.parquet.gzip")
#print(hap2021)

# 1. prosledis ime zemlje i vidis milionere za 3 godine koliko se bogatili
def get_billionaire_plot(country):
	bil2020 = pandas.read_parquet("../../CleansedRawData/cleanBillionaires2020.parquet.gzip")
	# print(bil2020)
	bil2021 = pandas.read_parquet("../../CleansedRawData/cleanBillionaires2021.parquet.gzip")
	# print(bil2021)
	bil2022 = pandas.read_parquet("../../CleansedRawData/cleanBillionaires2022.parquet.gzip")
	# print(bil2022)
	df2020 = bil2020[bil2020["Country"] == country]
	df1 = df2020.head(5)
	df2021 = bil2021[bil2021["Country"] == country]
	df2 = df2021.head(5)
	df2022 = bil2022[bil2022["Country"] == country]
	df3 = df2022.head(5)
	ax = plt.subplot()
	plt.scatter("Name", "NetWorth", data=df1)
	plt.title("Top 5 billionaires from " + country +" 2020.")
	plt.ylabel("billions")
	plt.setp(ax.get_xticklabels(), rotation=10, ha='right')
	plt.savefig("../../Figures/top5bil2020" + country + ".png")
	plt.show()
	ax = plt.subplot()
	plt.scatter("Name", "NetWorth", data=df2)
	plt.title("Top 5 billionaires from " + country +" 2021.")
	plt.ylabel("billions")
	plt.setp(ax.get_xticklabels(), rotation=10, ha='right')
	plt.savefig("../../Figures/top5bil2021" + country + ".png")
	plt.show()
	ax = plt.subplot()
	plt.scatter("Name", "NetWorth", data=df3)
	plt.title("Top 5 billionaires from " + country +" 2022.")
	plt.ylabel("billions")
	plt.setp(ax.get_xticklabels(), rotation=10, ha='right')
	plt.savefig("../../Figures/top5bil2022" + country + ".png")
	plt.show()

#test
#get_billionaire_plot("China")


# 2. prosledis ime zemlje i vidis koliko je kovid progresirao
def get_covid_plot(country):
	cov = pandas.read_parquet("../../CleansedRawData/cleanCovidData.parquet.gzip")
	df = cov[cov["location"] == country]
	df = df[df['date'].isin(["2020-12-31", "2021-12-31", "2022-09-20"])]
	print(df)
	df.plot(x="date", y=["total_cases_per_million", "total_deaths", "people_vaccinated", "people_fully_vaccinated"], kind="bar", figsize=(9, 8))
	plt.title(country)
	plt.show()

#get_covid_plot("China")

# 3. get top x countries by hap score
def get_happiness_plot(top_num):
	hap2019 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2019.parquet.gzip")
	# print(hap2019)
	hap2020 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2020.parquet.gzip")
	# print(hap2020)
	hap2021 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2021.parquet.gzip")
	df1 = hap2019.head(top_num)
	df1.GDP = df1.GDP.mul(10)
	df2 = hap2020.head(top_num)
	df3 = hap2021.head(top_num)
	#ax = plt.subplot()
	df1.plot(x="country", y=["GDP", "score"], kind="bar", figsize=(9, 8))
	plt.show()
	df2.plot(x="country", y=["GDP", "score"], kind="bar", figsize=(9, 8))
	plt.show()
	df3.plot(x="country", y=["GDP", "score"], kind="bar", figsize=(9, 8))
	plt.show()

def get_happiness_country_plot(country):
	hap2019 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2019.parquet.gzip")
	# print(hap2019)
	hap2020 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2020.parquet.gzip")
	# print(hap2020)
	hap2021 = pandas.read_parquet("../../CleansedRawData/cleanHappiness2021.parquet.gzip")
	df2019 = hap2019[hap2019["country"] == country]
	df2019.GDP = df2019.GDP.mul(10)
	df2019.country = "2019"
	df2020 = hap2020[hap2020["country"] == country]
	df2020.country = "2020"
	df2021 = hap2021[hap2021["country"] == country]
	df2021.country = "2021"

	df2 = df2019.append(df2020, ignore_index=True)
	df = df2.append(df2021, ignore_index=True)

	df.plot(x="country", y=["GDP", "score", "corruption", "freedomOfChoice"], kind="bar", figsize=(9, 8))
	plt.title(country)
	plt.show()

#get_happiness_plot(10)
#get_happiness_country_plot("Finland")


#caseclass2020.show()
df = caseclass2020[['location', 'score']]
df = df.dropna()
#df.show()

#sns.lmplot(x = 'location', y = 'score', data=df)
#plt.plot("location", "score", data=df)

#plt.show()