import pyspark as ps
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import to_date
import os,sys
from pyspark.sql.types import *

class CovidCaseClass:
    def __init__(self,spark):
        self.CovidJson = spark.read.json('../../CaseClassData/caseClass2020JSON.json')
        self.print_schema()

    def print_schema(self):
        self.CovidJson.printSchema()

    #def init_schema(self):
       # self.output_scema = StructType([
       #     StructField
       # ])


if __name__ == '__main__':
    spark = SparkSession.builder.appName("initClass").getOrCreate()
    CCC = CovidCaseClass(spark)

