import pyspark as ps
import pandas as pd
import numpy as np
import uuid

rawBillionaires2020 = pd.read_parquet("../../../RawDataParquet/rawBillionaires2020.parquet.gzip")
rawBillionaires2021 = pd.read_parquet("../../../RawDataParquet/rawBillionaires2021.parquet.gzip")
rawBillionaires2022 = pd.read_parquet("../../../RawDataParquet/rawBillionaires2022.parquet.gzip")

#Name,NetWorth,Country,Source,Rank,Age,Industry
#String,Double,String,String,String,Int,String

'''
TODO:

2020
     remove "" from Source
     remove $..B from NetWorth
     
2021 
    remove $..B from NetWorth
    remove "" from Source
    name upper case
    
2022
    rearrange columns
    cast age to int
    remove "" from Source
    finalWorth cast to double in billions and rename to NetWorth
    remove unused columns
    name upper case
    
'''

#2020
rawBillionaires2020['Source'].str.strip('"')
rawBillionaires2020.rename(columns={'Net Worth':'NetWorth'}, inplace=True)
rawBillionaires2020['NetWorth'].replace('\$','',regex=True,inplace=True)
rawBillionaires2020['NetWorth'].replace(" B",'',regex=True,inplace=True)
rawBillionaires2020['NetWorth'] = rawBillionaires2020['NetWorth'].astype(float)
rawBillionaires2020.rename(columns={'Country/Territory':'Country'}, inplace=True)
rawBillionaires2020.rename(columns={'Industries':'Industry'}, inplace=True)

#2021
rawBillionaires2021['Source'].str.strip('"')
rawBillionaires2021['NetWorth'].replace('\$','',regex=True,inplace=True)
rawBillionaires2021['NetWorth'].replace(" B",'',regex=True,inplace=True)
rawBillionaires2021['NetWorth'] = rawBillionaires2021['NetWorth'].astype(float)

#2022
rawBillionaires2022.rename(columns={'personName':'Name'}, inplace=True)
rawBillionaires2022.rename(columns={'finalWorth':'NetWorth'}, inplace=True)
rawBillionaires2022.rename(columns={'country':'Country'}, inplace=True)
rawBillionaires2022.rename(columns={'source':'Source'}, inplace=True)
rawBillionaires2022.rename(columns={'rank':'Rank'}, inplace=True)
rawBillionaires2022.rename(columns={'age':'Age'}, inplace=True)
rawBillionaires2022.rename(columns={'category':'Industry'}, inplace=True)
rawBillionaires2022 = rawBillionaires2022[[
     "Name", "NetWorth", "Country", "Source", "Rank", "Age", "Industry"
]]
rawBillionaires2022['NetWorth'] = rawBillionaires2022['NetWorth'].astype(float)
rawBillionaires2022['NetWorth'] = rawBillionaires2022['NetWorth'] / 1000

print(rawBillionaires2021['NetWorth'])
print(rawBillionaires2020['NetWorth'])
print(rawBillionaires2022['NetWorth'])

#TODO: Names all uppercased
#TODO: Add ids

rawBillionaires2020.to_parquet("../../../CleansedRawData/cleanBillionaires2020.parquet.gzip", compression='gzip')
rawBillionaires2021.to_parquet("../../../CleansedRawData/cleanBillionaires2021.parquet.gzip", compression='gzip')
rawBillionaires2022.to_parquet("../../../CleansedRawData/cleanBillionaires2022.parquet.gzip", compression='gzip')