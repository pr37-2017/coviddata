import pyspark as ps
import pandas as pd
import numpy as np
import uuid


rawCovidData = pd.read_parquet("../../RawDataParquet/rawCovidData.parquet.gzip")

#add row number column
rawCovidData['row_num'] = np.arange(len(rawCovidData))
rawCovidData['row_num'] = rawCovidData['row_num'].astype(str)
#print(rawCovidData)

'''
#seed to generate ids
columns = rawCovidData[['date', 'row_num']].agg(' '.join, 1).unique().tolist()

#generate ids
ids = np.random.randint(low=1e9, high=1e10, size=len(columns))

#map ids to columns
maps = {k:v for k,v in zip(columns,ids)}

#add id column
rawCovidData['id'] = rawCovidData[['date', 'row_num']].agg(' '.join, 1).map(maps)

print(rawCovidData)
'''

#generate unique uuid
rawCovidData['id'] = rawCovidData.apply(lambda _:uuid.uuid4(), axis=1)
#print(rawCovidData)

#test if its really unique
#print(rawCovidData['id'].is_unique)

duplicates = rawCovidData[rawCovidData.duplicated('id')]
#print(duplicates)


#select viable information
rawCovidDataSelection = rawCovidData[[
     "id", "iso_code", "continent",
     "location", "date", "total_cases",
     "new_cases", "total_deaths", "new_deaths",
     "total_cases_per_million", "new_cases_per_million",
     "total_deaths_per_million", "new_deaths_per_million",
     "total_tests", "new_tests", "total_tests_per_thousand", "new_tests_per_thousand",
     "total_vaccinations", "people_vaccinated", "people_fully_vaccinated", "total_boosters", "new_vaccinations"
]]

#set index to uuid
#rawCovidDataSelection.set_index('id', inplace=True)

#print(rawCovidDataSelection)

#take only rows without NaN values in important columns
cleanRawCovidData = rawCovidDataSelection[rawCovidDataSelection['date'].notna()]

#print(cleanRawCovidData)

cleanRawCovidData['id'] = cleanRawCovidData['id'].astype(str)

#TODO: Idea - dismiss all days without any new cases or (and) new deaths

cleanRawCovidData.to_parquet("../../CleansedRawData/cleanCovidData.parquet.gzip", compression='gzip')