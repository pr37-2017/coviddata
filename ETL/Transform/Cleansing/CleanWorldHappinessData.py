import pandas as pd
import pyspark as ps

#Overall rank,Country or region,Score,GDP per capita,Social support,Healthy life expectancy,Freedom to make life choices,Generosity,Perceptions of corruption
#Country name,Regional indicator,Ladder score,Standard error of ladder score,upperwhisker,lowerwhisker,Logged GDP per capita,Social support,Healthy life expectancy,Freedom to make life choices,Generosity,Perceptions of corruption,Ladder score in Dystopia,Explained by: Log GDP per capita,Explained by: Social support,Explained by: Healthy life expectancy,Explained by: Freedom to make life choices,Explained by: Generosity,Explained by: Perceptions of corruption,Dystopia + residual

#country, rank, GDP, score, lifeExpentancy, freedomOfChoice, corruption

#TODO:
# convert doubles precision

rawHappiness2018 = pd.read_parquet("../../../RawDataParquet/rawWorldHappiness2018.parquet.gzip")
rawHappiness2019 = pd.read_parquet("../../../RawDataParquet/rawWorldHappiness2019.parquet.gzip")
rawHappiness2020 = pd.read_parquet("../../../RawDataParquet/rawWorldHappiness2020.parquet.gzip")
rawHappiness2021 = pd.read_parquet("../../../RawDataParquet/rawWorldHappiness2021.parquet.gzip")

rawHappiness2018.rename(columns={'Country or region':'country'}, inplace=True)
rawHappiness2018.rename(columns={'Overall rank':'rank'}, inplace=True)
rawHappiness2018.rename(columns={'GDP per capita':'GDP'}, inplace=True)
rawHappiness2018.rename(columns={'Score':'score'}, inplace=True)
rawHappiness2018.rename(columns={'Healthy life expectancy':'lifeExpentancy'}, inplace=True)
rawHappiness2018.rename(columns={'Freedom to make life choices':'freedomOfChoice'}, inplace=True)
rawHappiness2018.rename(columns={'Perceptions of corruption':'corruption'}, inplace=True)
rawHappiness2018 = rawHappiness2018[[
     "country", "rank", "GDP", "score", "lifeExpentancy", "freedomOfChoice", "corruption"
]]

rawHappiness2019.rename(columns={'Country or region':'country'}, inplace=True)
rawHappiness2019.rename(columns={'Overall rank':'rank'}, inplace=True)
rawHappiness2019.rename(columns={'GDP per capita':'GDP'}, inplace=True)
rawHappiness2019.rename(columns={'Score':'score'}, inplace=True)
rawHappiness2019.rename(columns={'Healthy life expectancy':'lifeExpentancy'}, inplace=True)
rawHappiness2019.rename(columns={'Freedom to make life choices':'freedomOfChoice'}, inplace=True)
rawHappiness2019.rename(columns={'Perceptions of corruption':'corruption'}, inplace=True)
rawHappiness2019 = rawHappiness2019[[
     "country", "rank", "GDP", "score", "lifeExpentancy", "freedomOfChoice", "corruption"
]]

rawHappiness2020.rename(columns={'Country name':'country'}, inplace=True)
rawHappiness2020.rename(columns={'Regional indicator':'rank'}, inplace=True)
rawHappiness2020.rename(columns={'Logged GDP per capita':'GDP'}, inplace=True)
rawHappiness2020.rename(columns={'Ladder score':'score'}, inplace=True)
rawHappiness2020.rename(columns={'Healthy life expectancy':'lifeExpentancy'}, inplace=True)
rawHappiness2020.rename(columns={'Freedom to make life choices':'freedomOfChoice'}, inplace=True)
rawHappiness2020.rename(columns={'Perceptions of corruption':'corruption'}, inplace=True)
rawHappiness2020 = rawHappiness2020[[
     "country", "rank", "GDP", "score", "lifeExpentancy", "freedomOfChoice", "corruption"
]]

rawHappiness2021.rename(columns={'Country name':'country'}, inplace=True)
rawHappiness2021.rename(columns={'Regional indicator':'rank'}, inplace=True)
rawHappiness2021.rename(columns={'Logged GDP per capita':'GDP'}, inplace=True)
rawHappiness2021.rename(columns={'Ladder score':'score'}, inplace=True)
rawHappiness2021.rename(columns={'Healthy life expectancy':'lifeExpentancy'}, inplace=True)
rawHappiness2021.rename(columns={'Freedom to make life choices':'freedomOfChoice'}, inplace=True)
rawHappiness2021.rename(columns={'Perceptions of corruption':'corruption'}, inplace=True)
rawHappiness2021 = rawHappiness2021[[
     "country", "rank", "GDP", "score", "lifeExpentancy", "freedomOfChoice", "corruption"
]]

#TODO: Add ids

rawHappiness2021.to_parquet("../../../CleansedRawData/cleanHappiness2021.parquet.gzip", compression='gzip')
rawHappiness2020.to_parquet("../../../CleansedRawData/cleanHappiness2020.parquet.gzip", compression='gzip')
rawHappiness2019.to_parquet("../../../CleansedRawData/cleanHappiness2019.parquet.gzip", compression='gzip')
rawHappiness2018.to_parquet("../../../CleansedRawData/cleanHappiness2018.parquet.gzip", compression='gzip')