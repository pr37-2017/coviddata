import pandas as pd
import pyspark as ps

rawCovidData = pd.read_csv("../../RawData/owid-covid-data.csv")
rawBillionaires2020 = pd.read_csv("../../RawData/Forbes Billionaire 2020.csv")
rawBillionaires2021 = pd.read_csv("../../RawData/Forbes Billionaire 2021.csv")
rawBillionaires2022 = pd.read_csv("../../RawData/Forbes Billionaire 2022.csv")
rawWorldHappiness2018 = pd.read_csv("../../RawData/World Happiness 2018.csv")
rawWorldHappiness2019 = pd.read_csv("../../RawData/World Happiness 2019.csv")
rawWorldHappiness2020 = pd.read_csv("../../RawData/World Happiness 2020.csv")
rawWorldHappiness2021 = pd.read_csv("../../RawData/World Happiness 2021.csv")

rawCovidData.to_parquet("../../RawDataParquet/rawCovidData.parquet.gzip", compression='gzip')
rawBillionaires2020.to_parquet("../../RawDataParquet/rawBillionaires2020.parquet.gzip", compression='gzip')
rawBillionaires2021.to_parquet("../../RawDataParquet/rawBillionaires2021.parquet.gzip", compression='gzip')
rawBillionaires2022.to_parquet("../../RawDataParquet/rawBillionaires2022.parquet.gzip", compression='gzip')
rawWorldHappiness2018.to_parquet("../../RawDataParquet/rawWorldHappiness2018.parquet.gzip", compression='gzip')
rawWorldHappiness2019.to_parquet("../../RawDataParquet/rawWorldHappiness2019.parquet.gzip", compression='gzip')
rawWorldHappiness2020.to_parquet("../../RawDataParquet/rawWorldHappiness2020.parquet.gzip", compression='gzip')
rawWorldHappiness2021.to_parquet("../../RawDataParquet/rawWorldHappiness2021.parquet.gzip", compression='gzip')

#test = pd.read_parquet("../../RawDataParquet/rawCovidData.parquet.gzip")

#print(test)

