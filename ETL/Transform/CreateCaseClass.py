import pandas as pd
import pyspark as ps
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import to_date

#connect all in one parquet
#1. in WorldHappines DF create a column with a list of all billionaires from that country
#2. do it for each year of WOrldHappiness
#3. join to covid data by YEAR and COUNTRY ????? maybe not

# COUNTRY / YEAR 18-22 / listOfBillionaires / WH columns
# outerjoin

#create pyspark DF from pandas DF - pyspark allows paralelization, works on multiple machines and nodes
#
#   pysparkDF = spark.createDataFrame(pandasDF)

spark = SparkSession.builder.appName('caseClassCreation').getOrCreate()

Billionaires2020 = spark.read.parquet("../../CleansedRawData/cleanBillionaires2020.parquet.gzip")
Billionaires2021 = spark.read.parquet("../../CleansedRawData/cleanBillionaires2021.parquet.gzip")
Billionaires2022 = spark.read.parquet("../../CleansedRawData/cleanBillionaires2022.parquet.gzip")

WorldHappiness2018 = spark.read.parquet("../../CleansedRawData/cleanHappiness2018.parquet.gzip")
WorldHappiness2019 = spark.read.parquet("../../CleansedRawData/cleanHappiness2019.parquet.gzip")
WorldHappiness2020 = spark.read.parquet("../../CleansedRawData/cleanHappiness2020.parquet.gzip")
WorldHappiness2021 = spark.read.parquet("../../CleansedRawData/cleanHappiness2021.parquet.gzip")

CovidData = spark.read.parquet("../../CleansedRawData/cleanCovidData.parquet.gzip")

CovidData2020 = CovidData.filter(to_date(CovidData["date"]).between("2020-01-01","2020-12-30"))
CovidData2021 = CovidData.filter(to_date(CovidData["date"]).between("2021-01-01","2021-12-30"))
#CovidData2020.show(truncate=False)

#Name,NetWorth,Country,Source,Rank,Age,Industry
billionairesGrouped2020 = Billionaires2020.groupBy('Country').agg(F.collect_list(F.struct("Name","NetWorth","Country","Source","Rank","Age","Industry")).alias("Billionaires"))
billionairesGrouped2021 = Billionaires2021.groupBy('Country').agg(F.collect_list(F.struct("Name","NetWorth","Country","Source","Rank","Age","Industry")).alias("Billionaires"))
billionairesGrouped2022 = Billionaires2022.groupBy('Country').agg(F.collect_list(F.struct("Name","NetWorth","Country","Source","Rank","Age","Industry")).alias("Billionaires"))

#billionairesGrouped2020.show(truncate=False)

groupedCovidData2020 = CovidData2020.groupBy('location').agg(F.collect_list(F.struct("id", "iso_code", "continent",
     "location", "date", "total_cases",
     "new_cases", "total_deaths", "new_deaths",
     "total_cases_per_million", "new_cases_per_million",
     "total_deaths_per_million", "new_deaths_per_million",
     "total_tests", "new_tests", "total_tests_per_thousand", "new_tests_per_thousand",
     "total_vaccinations", "people_vaccinated", "people_fully_vaccinated", "total_boosters", "new_vaccinations")).alias("CovidData"))


groupedCovidData2021 = CovidData2021.groupBy('location').agg(F.collect_list(F.struct("id", "iso_code", "continent",
     "location", "date", "total_cases",
     "new_cases", "total_deaths", "new_deaths",
     "total_cases_per_million", "new_cases_per_million",
     "total_deaths_per_million", "new_deaths_per_million",
     "total_tests", "new_tests", "total_tests_per_thousand", "new_tests_per_thousand",
     "total_vaccinations", "people_vaccinated", "people_fully_vaccinated", "total_boosters", "new_vaccinations")).alias("CovidData"))

#TODO: add countOfBillionaires column
#TODO: add missing datasets
#TODO: all column names uppercase

HappinessBillionaires2020 = WorldHappiness2020.join(billionairesGrouped2020, ['Country'],"leftouter")
HappinessBillionaires2021 = WorldHappiness2021.join(billionairesGrouped2021, ['Country'],"leftouter")

CaseClass2020 = groupedCovidData2020.join(HappinessBillionaires2020,groupedCovidData2020['location'] == HappinessBillionaires2020['Country'],"leftouter")

#CaseClass2020.filter(CaseClass2020["location"] == "Serbia").show()
#CaseClass2020.filter(CaseClass2020["location"] == "Montenegro").show()
#CaseClass2020.filter(CaseClass2020["location"] == "Croatia").show()
#CaseClass2020.filter(CaseClass2020["location"] == "China").show()

#HappinessBillionaires2020.show(truncate=False)

#groupedCovidData2020.show(truncate=False)

CaseClass2020.show()
#CovidData.show(truncate=False)

#CaseClass2020.write.parquet("../../CaseClassData/caseClass2020.parquet.gzip")

#CaseClass2020.write.json("../../CaseClassData/caseClass2020JSON.json")




