import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType

spark = SparkSession.builder\
	.master("local").appName("hdfs_test").getOrCreate()

caseclass2020 = spark.read.json("../../CaseClassData/caseClass2020JSON.json")

caseclass2020.write.json("hdfs://localhost:9000/coviddata1.json")

df_load = spark.read.csv('hdfs://localhost:9000/coviddata1.json')
df_load.show()