from elasticsearch import Elasticsearch
from elasticsearch import helpers
from pyspark.sql import SparkSession
import numpy as np
import pandas as pd
import sys
import json
from ETL.Transform.CaseClass import CovidCaseClass
#import elasticsearch_loader as loader


spark = SparkSession.builder.appName('loadingToElastic').getOrCreate()

caseClass2020 = spark.read.parquet("../../CaseClassData/caseClass2020.parquet.gzip")
#caseClass2020 = spark.read.json("../../CaseClassData/caseClass2020JSON.json")

es = Elasticsearch("http://localhost:9200")

data = CovidCaseClass(spark)


#es.info()

#doc = {
# "author": "document-author",
#"text": "Covid mixed data"
#}

#res = es.index(index="sample-index", id=2, body=doc)
#print(res['result'])

#es = loader.Elasticsearch("http://localhost:9200")
#es.index(index="covid-data", id=2, body=caseClass2020)

'''
CaseClass = open("../../CaseClassData/covid2020.json", 'r').read()
ClearData = CaseClass.splitlines(True)
i = 0
json_str = ""
docs ={}
for line in ClearData:
    line = ''.join(line.split())
    if line != "},":
        json_str = json_str+line
    else:
        docs[i] = json_str+"}"
        json_str = ""
        print(docs[i])
        es.index(index="coviddata2020",id=i, body=docs[i])
        i=i+1
'''

'''
i =1

CaseClass = open("../../CaseClassData/covid2020.json")
docket_content = CaseClass.read()
print(docket_content)
#es.index(index='coviddata2020', ignore=400,doc_type='docket',id=i,body=json.loads(docket_content))
'''

'''
df = caseClass2020.drop('_id')
df.write.format('org.elasticsearch.spark.sql').option('es.nodes', 'localhost').option('es.port', 9200).option('es.resource', '%s/%s' % ('coviddata2020', 'docket')).save()
'''

use_these_keys = ['id','FirstName']

def filterKeys(document):
    return {key: document[key] for key in use_these_keys}


def doc_generator(df):
    df_iter = df.iterrows()
    for index, document in df_iter:
        yield {
            "_index": 'index',
            "_type": "_doc",
            "_id": f"{document['id']}",
            "_source": filterKeys(document)
        }
        raise StopIteration


helpers.bulk(es,doc_generator(data.CovidJson))

